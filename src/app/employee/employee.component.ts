import { Component } from '@angular/core';

@Component({
  selector: 'page-employee',
  templateUrl: './employee.component.html',
})
export class EmployeeComponent  { 
	//public employees : any;
	employees = [{
		id:1,
		name:'Jhon',
		phone : '9877374356',
		address : {
			city : 'Pune',
			address1 : 'acb road',
			address2 : 'xyz building',
			postCode : '411332'
		}
	},{
		id:2,
		name:'Jackol',
		phone : '9876567890',
		address : {
			city : 'Pune',
			address1 : 'pqr road',
			address2 : 'wxy building',
			postCode : '421672'
		}
		
	},{
		id:3,
		name:'Tapaswi',
		phone : '9665562728',
		address : {
			city : 'Pune',
			address1 : 'medankarwadi road',
			address2 : 'pqr building',
			postCode : '410501'
		}
		
	},{
		id:4,
		name:'Tejaswi',
		phone : '9975801705',
		address:{
			city : 'Mumbai',
			address1 : 'vashi road',
			address2 : 'xyz building',
			postCode : '411889'
		}
	},{
		id:5,
		name:'Rahul',
		phone : '9975587878',
		address : {
			city : 'Mumbai',
			address1 : 'vashi road',
			address2 : 'lmn building',
			postCode : '411889'
		}
	}]
	
}
