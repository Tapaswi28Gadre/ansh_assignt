import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  templateUrl: './employee/employee.component.html'
})
export class AppComponent  { 

//name = 'Angular'; 
public employee : any;
public searchText : any;
constructor(){
	this.employees = [{
		id:1,
		name:'Jhon',
		phone : '9877374356',
		address : {
			city : 'Pune',
			address1 : 'acb road',
			address2 : 'xyz building',
			postalCode : '411332'
		},
		editMode : false
	},{
		id:2,
		name:'Jackol',
		phone : '9876567890',
		address : {
			city : 'Pune',
			address1 : 'pqr road',
			address2 : 'wxy building',
			postalCode : '421672'
		},
		editMode : false
		
	},{
		id:3,
		name:'Tapaswi',
		phone : '9665562728',
		address : {
			city : 'Pune',
			address1 : 'medankarwadi road',
			address2 : 'pqr building',
			postalCode : '410501'
		},
		editMode : false
		
	},{
		id:4,
		name:'Tejaswi',
		phone : '9975801705',
		address:{
			city : 'Mumbai',
			address1 : 'vashi road',
			address2 : 'xyz building',
			postalCode : '411889'
		},
		editMode : false
	},{
		id:5,
		name:'Rahul',
		phone : '9975587878',
		address : {
			city : 'Mumbai',
			address1 : 'vashi road',
			address2 : 'lmn building',
			postalCode : '411889'
		},
		editMode : false
	}]
}

	addNewEmployee(emp){
		console.log('new added'+emp);
		emp.editMode = true;

	}
	addEmployee(){
		console.log('new added');
	}
	saveEmployee(emp){
		console.log('edited'+JSON.stringify(emp));
		emp.editMode = false;
	}
	editEmployee(id)){
		console.log('edit added');
		
	}

}


}
