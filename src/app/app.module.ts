import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent }  from './app.component';
import { EmployeeComponent } from './employee/employee.component';
import { AddEmpComponent } from './add-employee/add-employee.component';
import { FormsModule }   from '@angular/forms';

const appRoutes: Routes = [
  { path: 'employees', component: EmployeeComponent },
  { path: 'employee/add', component: AddEmpComponent },
  //{ path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports:      [ 
  	BrowserModule ,
  	FormsModule,
  	RouterModule.forRoot(appRoutes)
  	],
  declarations: [ AppComponent, EmployeeComponent,AddEmpComponent],
  bootstrap:    [ AppComponent ]
})
export class AppModule { 

}
